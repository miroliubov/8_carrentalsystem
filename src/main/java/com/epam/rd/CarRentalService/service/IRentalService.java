package com.epam.rd.CarRentalService.service;

import com.epam.rd.CarRentalService.entity.Car;

import java.util.List;

public interface IRentalService {

    List<Car> getAllAvailableCars();
}
