package com.epam.rd.CarRentalService.service;

import com.epam.rd.CarRentalService.dao.ICarDao;
import com.epam.rd.CarRentalService.entity.Car;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class RentalService implements IRentalService {

    @Autowired
    private ICarDao carDao;

    @Override
    public List<Car> getAllAvailableCars() {
        return carDao.getAll().stream()
                .filter(x -> 1 == x.getFlagActive())
                .collect(Collectors.toList());
    }




}
