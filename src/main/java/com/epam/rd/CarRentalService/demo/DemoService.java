package com.epam.rd.CarRentalService.demo;

import com.epam.rd.CarRentalService.service.IRentalService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class DemoService {

    @Autowired
    private IRentalService rentalService;

    public void execute() {
        System.out.println(rentalService.getAllAvailableCars());
    }

}
