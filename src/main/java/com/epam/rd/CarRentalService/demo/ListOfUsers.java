package com.epam.rd.CarRentalService.demo;

import com.epam.rd.CarRentalService.entity.User;

public class ListOfUsers {
    public static User[] USERS = new User[]{
            new User("user", "Petr2018", "qwerty12345", "petr@gmail.com", "Petr", "Petrov", "224567342", "12.03.1995", 10000.0),
            new User("user", "Oleg18rus", "qwerty12345", "oleg@gmail.com", "Oleg", "Olegov", "113569342", "27.08.1989", 500.0),
    };
}
