package com.epam.rd.CarRentalService.demo;

import com.epam.rd.CarRentalService.entity.Car;

public class ListOfCars {

    public static Car[] CARS = new Car[]{
            new Car(1,"Audi A4", 300, 1),
            new Car(2,"BMW E32", 400, 0),
            new Car(3,"Lada X-RAY", 200, 1),
            new Car(4,"Mazda 3", 250, 1)
    };
}
