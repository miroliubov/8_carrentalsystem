package com.epam.rd.CarRentalService.entity;

public enum ClientRole {

    GUEST,

    USER,

    ADMIN

}
