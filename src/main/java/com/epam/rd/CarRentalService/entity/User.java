package com.epam.rd.CarRentalService.entity;

import java.util.Objects;
import org.apache.log4j.Logger;

public class User {

    static Logger logger = Logger.getLogger(User.class);

    private ClientRole clientRole;
    private String login;
    private String password;
    private String email;
    private String name;
    private String surname;
    private String passportNumber;
    private String birthDate;
    private double balance;

    public User(String clientRole, String login, String password, String email, String name, String surname, String passportNumber, String birthDate, double balance) {
        try {
            setClientRole(clientRole);
            this.login = login;
            this.password = password;
            this.email = email;
            this.name = name;
            this.surname = surname;
            this.passportNumber = passportNumber;
            this.birthDate = birthDate;
            this.balance = balance;
        } catch (Exception e) {
            logger.info("Exception while creating User: " + e);
        }
    }

    public ClientRole getClientRole() {
        return clientRole;
    }

    public void setClientRole(String clientRole) {
        if (clientRole.toUpperCase().equals("ADMIN")) {
            this.clientRole = ClientRole.ADMIN;
        } else {
            this.clientRole = ClientRole.USER;
        }
    }

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public String getPassportNumber() {
        return passportNumber;
    }

    public void setPassportNumber(String passportNumber) {
        this.passportNumber = passportNumber;
    }

    public String getBirthDate() {
        return birthDate;
    }

    public void setBirthDate(String birthDate) {
        this.birthDate = birthDate;
    }

    public double getBalance() {
        return balance;
    }

    public void setBalance(double balance) {
        this.balance = balance;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        User user = (User) o;
        return Double.compare(user.balance, balance) == 0 &&
                clientRole == user.clientRole &&
                Objects.equals(login, user.login) &&
                Objects.equals(password, user.password) &&
                Objects.equals(email, user.email) &&
                Objects.equals(name, user.name) &&
                Objects.equals(surname, user.surname) &&
                Objects.equals(passportNumber, user.passportNumber) &&
                Objects.equals(birthDate, user.birthDate);
    }

    @Override
    public int hashCode() {
        return Objects.hash(clientRole, login, password, email, name, surname, passportNumber, birthDate, balance);
    }

    @Override
    public String toString() {
        return "User{" +
                "clientRole=" + clientRole +
                ", login='" + login + '\'' +
                ", password='" + password + '\'' +
                ", email='" + email + '\'' +
                ", name='" + name + '\'' +
                ", surname='" + surname + '\'' +
                ", passportNumber='" + passportNumber + '\'' +
                ", birthDate='" + birthDate + '\'' +
                ", balance=" + balance +
                '}';
    }
}
