package com.epam.rd.CarRentalService.entity;

import java.util.Objects;

public class Car {

    private String name;
    private int price;
    private int flagActive;
    private int id;

    public Car(int id, String name, int price, int flagActive) {
        this.id = id;
        this.name = name;
        this.price = price;
        this.flagActive = flagActive;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getPrice() {
        return price;
    }

    public void setPrice(int price) {
        this.price = price;
    }

    public int getFlagActive() {
        return flagActive;
    }

    public void setFlagActive(int flagActive) {
        this.flagActive = flagActive;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Car car = (Car) o;
        return price == car.price &&
                flagActive == car.flagActive &&
                Objects.equals(name, car.name);
    }

    @Override
    public int hashCode() {
        return Objects.hash(name, price, flagActive);
    }

    @Override
    public String toString() {
        return "Car{" +
                "name='" + name + '\'' +
                ", price=" + price +
                ", flagActive=" + flagActive +
                '}';
    }
}
