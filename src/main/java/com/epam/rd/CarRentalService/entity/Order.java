package com.epam.rd.CarRentalService.entity;

import java.util.Objects;

public class Order {

    private long orderID;
    private long carID;
    private long userID;
    private double rentPrice;
    private double price;
    private int period;

    public Order(long orderID, long carID, long userID, double rentPrice, double price, int period) {
        this.orderID = orderID;
        this.carID = carID;
        this.userID = userID;
        this.rentPrice = rentPrice;
        this.price = price;
        this.period = period;
    }

    public long getOrderID() {
        return orderID;
    }

    public void setOrderID(long orderID) {
        this.orderID = orderID;
    }

    public long getCarID() {
        return carID;
    }

    public void setCarID(long carID) {
        this.carID = carID;
    }

    public long getUserID() {
        return userID;
    }

    public void setUserID(long userID) {
        this.userID = userID;
    }

    public double getRentPrice() {
        return rentPrice;
    }

    public void setRentPrice(double rentPrice) {
        this.rentPrice = rentPrice;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public int getPeriod() {
        return period;
    }

    public void setPeriod(int period) {
        this.period = period;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Order order = (Order) o;
        return orderID == order.orderID &&
                carID == order.carID &&
                userID == order.userID &&
                Double.compare(order.rentPrice, rentPrice) == 0 &&
                Double.compare(order.price, price) == 0 &&
                period == order.period;
    }

    @Override
    public int hashCode() {
        return Objects.hash(orderID, carID, userID, rentPrice, price, period);
    }

    @Override
    public String toString() {
        return "Order{" +
                "orderID=" + orderID +
                ", carID=" + carID +
                ", userID=" + userID +
                ", rentPrice=" + rentPrice +
                ", price=" + price +
                ", period=" + period +
                '}';
    }
}
