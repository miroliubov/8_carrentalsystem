package com.epam.rd.CarRentalService.dao;

import com.epam.rd.CarRentalService.entity.User;

import java.util.List;

public interface IUserDao {
    void createUser(User user);
    List<User> getAllUser();
    void updateUser(User oldUser, User newUser);
    void deleteUser(User user);
}
