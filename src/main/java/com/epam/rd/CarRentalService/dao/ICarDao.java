package com.epam.rd.CarRentalService.dao;

import com.epam.rd.CarRentalService.entity.Car;
import java.util.List;

public interface ICarDao {

    List<Car> getAll();

    void create(Car car);

    void deleteCar(Car car);
}
