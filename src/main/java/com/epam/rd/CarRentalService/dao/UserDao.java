package com.epam.rd.CarRentalService.dao;

import com.epam.rd.CarRentalService.demo.ListOfUsers;
import com.epam.rd.CarRentalService.entity.User;
import org.springframework.stereotype.Repository;

import javax.annotation.PostConstruct;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

@Repository
public class UserDao implements IUserDao{

    List<User> users = new ArrayList<>();

    @PostConstruct
    private void usersFill() {
        users.addAll(Arrays.asList(ListOfUsers.USERS));
    }

    @Override
    public void createUser(User user) {
        if (user != null) {
            users.add(user);
        }
    }

    @Override
    public List<User> getAllUser() {
        return users;
    }

    @Override
    public void updateUser(User oldUser, User newUser) {
        if (oldUser != null && newUser != null) {
            users.remove(oldUser);
            users.add(newUser);
        }
    }

    @Override
    public void deleteUser(User user) {
        if (user != null) {
            users.remove(user);
        }
    }
}
