package com.epam.rd.CarRentalService.dao;

import com.epam.rd.CarRentalService.demo.ListOfCars;
import com.epam.rd.CarRentalService.entity.Car;
import org.springframework.stereotype.Repository;

import javax.annotation.PostConstruct;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

@Repository
public class CarDao implements ICarDao {

    private List<Car> cars = new ArrayList<>();

    @PostConstruct
    private void carsFill() {
        cars.addAll(Arrays.asList(ListOfCars.CARS));
    }

    @Override
    public List<Car> getAll() {
        return cars;
    }

    @Override
    public void create(Car car) {
        if (car != null) {
            cars.add(car);
        }
    }

    @Override
    public void deleteCar(Car car) {
        if (car != null) {
            cars.remove(car);
        }
    }
}
