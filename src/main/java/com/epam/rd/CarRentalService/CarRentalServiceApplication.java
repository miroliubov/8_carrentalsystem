package com.epam.rd.CarRentalService;

import com.epam.rd.CarRentalService.demo.DemoService;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ApplicationContext;

@SpringBootApplication
public class CarRentalServiceApplication {

	public static void main(String[] args) {
		ApplicationContext ctx = SpringApplication.run(CarRentalServiceApplication.class, args);

		DemoService demo = ctx.getBean(DemoService.class);
		demo.execute();
	}

}

